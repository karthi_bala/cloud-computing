const express = require('express')
const app = express()
var port = process.env.PORT || 8080 ; //important for deployment later
app.use (express.urlencoded({extended: false}))
const cors = require('cors'); //New for microservice
app.use(cors()); //New for microservice
app.listen(port)
console.log("US City Search Microservice is running on port " + port)
app.get("/", (req, res)=>{
    res.send("US City Search Microservice by Karthi Balasundaram");
})
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://CCA-balasundaramk1:Smee2023@cca-balasundaramk1.alwae.mongodb.net/cca-labs?retryWrites=true&w=majority --collection uscities --file uscities.csv --format csv";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to MongoDB cluster");
});
app.get('/uscities-search/:zips(\\d{1,5})', function (req, res) {
    const db = dbClient.db();
    let zipRegEx = new RegExp(req.params.zips);
    let fields = {_id: false, 
                    zips : true, 
                    city : true, 
                    state_id: true, 
                    state_name: true,
                    county_name:true,
                    timezone: true};
    const cursor = db.collection("uscities").find({zips:zipRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results); // output all records for debug only
        res.send(results);
    });
});
app.get('/uscities-search/:city', function (req, res) {
    const db = dbClient.db();
    let cityRegEx = new RegExp(req.params.city, 'i');
    let fields = {_id: false, 
                    zips : true, 
                    city : true, 
                    state_id: true, 
                    state_name: true,
                    county_name:true,
                    timezone: true};
    const cursor = db.collection("uscities").find({city:cityRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results); // output all records
        res.send(results);
    });
});

